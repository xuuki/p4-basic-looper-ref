//
//  LooperGui.h
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#ifndef H_LooperGui
#define H_LooperGui

#include "../JuceLibraryCode/JuceHeader.h"
#include "../audio/Looper.h"

/** Gui for the looper class */
class LooperGui :   public Component,
                    public Button::Listener
{
public:
    /** Constructor - receives a reference to a Looper object to control */
    LooperGui (Looper& l);
    /** Destructor */
    ~LooperGui();
    
    void buttonClicked (Button* button) override;
    void resized() override;
private:
    Looper& looper;                 //reference to a looper object
    TextButton playButton;
    TextButton recordButton;
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (LooperGui)
};

#endif
